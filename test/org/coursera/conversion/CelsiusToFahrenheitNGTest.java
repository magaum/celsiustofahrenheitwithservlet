package org.coursera.conversion;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CelsiusToFahrenheitNGTest {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
	    System.setProperty("webdriver.gecko.driver","/opt/geckodriver");
		driver = new FirefoxDriver();
		baseUrl = "https://www.katalon.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testCelsiusToFahrenheit() throws Exception {
		driver.get("http://localhost:8080/");
		driver.findElement(By.name("value")).click();
		driver.findElement(By.name("value")).clear();
		driver.findElement(By.name("value")).sendKeys("100");
		driver.findElement(
				By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Welcome!'])[1]/following::input[2]"))
				.click();
		driver.findElement(By.xpath("//html")).click();
		assertEquals(driver.findElement(By.xpath("//p")).getText(), "The value is: 212");
	}
	
	@Test
	public void testFahrenheitToCelsius() throws Exception {
	    driver.get("http://localhost:8080/");
	    new Select(driver.findElement(By.name("choice"))).selectByVisibleText("Fahrenheit to Celsius");
	    driver.findElement(By.name("choice")).click();
	    driver.findElement(By.name("value")).click();
	    driver.findElement(By.name("value")).clear();
	    driver.findElement(By.name("value")).sendKeys("212");
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Welcome!'])[1]/following::input[2]")).click();
	    assertEquals(driver.findElement(By.xpath("//p")).getText(), "The value is: 100");
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
