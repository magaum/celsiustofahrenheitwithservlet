package org.coursera.conversion;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ConversionServlet
 */
@WebServlet("/convert")
public class ConversionServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private Conversion conversion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConversionServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Integer value;
		Integer valueConverted;
		try{
			value = Integer.parseInt(request.getParameter("value"));
		}catch(NullPointerException | NumberFormatException  e) {
			value = null;
		}
		if( "fahrenheit".equalsIgnoreCase(request.getParameter("choice")) ) {
			conversion = new FahrenheitToCelsiusConversion();
			valueConverted = conversion.convert(value);
		}else {
			conversion = new CelsiusToFahrenheitConversion();
			valueConverted = conversion.convert(value);
		}
		
		showAnswer(response, valueConverted);
	}

	public void showAnswer(HttpServletResponse response, Integer value) {
		try(PrintWriter writer = response.getWriter()){
			writer.println(""
					+ "<!DOCTYPE html>"
					+ "<head>"
					+ "<meta charset=\"UTF-8\""
					+ "<title> Conversion Result"
					+ "</head>"
					+ "<body>"
					+ "<p>The value is: "+value+"</p>"
					+ "</body"
			);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
