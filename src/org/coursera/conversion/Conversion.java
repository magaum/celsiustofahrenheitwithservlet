package org.coursera.conversion;

public interface Conversion {
	
	int convert(int value);

}
