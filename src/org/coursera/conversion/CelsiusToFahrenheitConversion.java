package org.coursera.conversion;

public class CelsiusToFahrenheitConversion implements Conversion {

	@Override
	public int convert(int celsius) {
		return ((celsius * 9) / 5) + 32;
	}

}
