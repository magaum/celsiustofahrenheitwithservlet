package org.coursera.conversion;

public class FahrenheitToCelsiusConversion implements Conversion {

	@Override
	public int convert(int fahrenheit) {
		return ((fahrenheit - 32) * 5) / 9;
	}

}
